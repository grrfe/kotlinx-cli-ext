plugins {
    kotlin("jvm")
    id("net.nemerosa.versioning") version "3.1.0"
    `maven-publish`
}

group = "fe.kotlinx-cli-ext"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
}

kotlin {
    jvmToolchain(17)
}

dependencies {
    api(platform("com.github.1fexd:super"))
    api("org.jetbrains.kotlinx:kotlinx-cli-jvm")
    testImplementation(kotlin("test"))
}


publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}
