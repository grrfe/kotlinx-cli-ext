package fe.kotlinx.cli.command

import kotlinx.cli.ArgParser

class DeferredSubcommandArgParser(
    private val argParser: ArgParser,
    private val deferredSubcommands: Map<String, DeferredSubcommand>
) {
    fun parse(args: Array<String>): DeferredSubcommand? {
        val result = argParser.parse(args)
        if (result.commandName != argParser.programName) {
            return deferredSubcommands[result.commandName]
        }

        return null
    }
}
