package fe.kotlinx.cli.command

import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand

@OptIn(ExperimentalCli::class)
abstract class DeferredSubcommand(name: String, actionDescription: String) : Subcommand(name, actionDescription) {
    override fun execute() {}
}

abstract class DeferredSubcommand0(
    name: String, actionDescription: String
) : DeferredSubcommand(name, actionDescription) {
    abstract fun executeDeferred()
}

abstract class DeferredSubcommand1<T>(
    name: String, actionDescription: String
) : DeferredSubcommand(name, actionDescription) {
    abstract fun executeDeferred(t: T)
}

abstract class DeferredSubcommand2<T, T2>(
    name: String, actionDescription: String
) : DeferredSubcommand(name, actionDescription) {
    abstract fun executeDeferred(t: T, t2: T2)
}

abstract class DeferredSubcommand3<T, T2, T3>(
    name: String, actionDescription: String
) : DeferredSubcommand(name, actionDescription) {
    abstract fun executeDeferred(t: T, t2: T2, t3: T3)
}
