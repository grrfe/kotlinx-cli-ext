package fe.kotlinx.cli.ext

import fe.kotlinx.cli.command.DeferredSubcommandArgParser
import fe.kotlinx.cli.command.DeferredSubcommand
import kotlinx.cli.ArgParser
import kotlinx.cli.ExperimentalCli


class DeferredSubcommandScope {
    private val deferred = mutableMapOf<String, DeferredSubcommand>()

    fun <T : DeferredSubcommand> deferred(vararg deferredSubcommands: T) {
        deferredSubcommands.forEach { deferredSubcommand ->
            if (deferred.containsKey(deferredSubcommand.name)) error("A deferred subcommand with this name has already been registered")
            deferred[deferredSubcommand.name] = deferredSubcommand
        }
    }

    fun build(): Map<String, DeferredSubcommand> = deferred
}


@OptIn(ExperimentalCli::class)
fun ArgParser.withDeferredSubcommands(fn: DeferredSubcommandScope.() -> Unit): DeferredSubcommandArgParser {
    val deferredSubcommands = DeferredSubcommandScope().apply(fn).build()
    this.subcommands(*deferredSubcommands.values.toTypedArray())

    return DeferredSubcommandArgParser(this, deferredSubcommands)
}
