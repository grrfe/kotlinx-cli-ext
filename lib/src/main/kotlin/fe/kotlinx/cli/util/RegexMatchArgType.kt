package fe.kotlinx.cli.util

import kotlinx.cli.ArgType
import kotlinx.cli.ParsingException

open class RegexMatchArgType(
    private val regex: Regex
) : ArgType<List<String>>(true) {
    override val description = "{ String }"
    override fun convert(value: kotlin.String, name: kotlin.String): List<kotlin.String> {
        val result = regex.matchEntire(value)
            ?: throw ParsingException("Option $name's value ($value) does not match regex $regex")

        return result.groupValues
    }
}
