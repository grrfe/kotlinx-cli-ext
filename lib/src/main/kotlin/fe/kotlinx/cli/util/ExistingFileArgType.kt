package fe.kotlinx.cli.util

import kotlinx.cli.ArgType
import kotlinx.cli.ParsingException
import java.io.File

enum class DirectoryContent {
    Empty, NotEmpty
}

sealed interface FileType {
    data object File : FileType
    data class Directory(val directoryContent: DirectoryContent? = null) : FileType
}

open class ExistingFileArgType(
    private val fileType: FileType = FileType.File
) : ArgType<File>(true) {
    override val description = "{ File }"
    override fun convert(value: kotlin.String, name: kotlin.String): File {
        val file = File(value)
        if (!file.exists()) {
            throw ParsingException("Option $name is expected to be a path to an existing file/directory. $value is provided.")
        }

        val isFile = file.isFile
        val isDirectory = file.isDirectory

        if (!isFile && !isDirectory) {
            throw ParsingException("Option $name is expected to be a path to an existing file/directory. $value is neither a file nor a directory.")
        }

        if (fileType is FileType.File && !isFile) {
            throw ParsingException("File specified by $name exists, but is not a file.")
        }

        if (fileType is FileType.Directory) {
            if (!isDirectory) {
                throw ParsingException("File specified by $name exists, but is not a directory.")
            }

            if (fileType.directoryContent != null) {
                val hasContent = file.listFiles() != null
                if (fileType.directoryContent == DirectoryContent.Empty && hasContent) {
                    throw ParsingException("Directory specified by $name is not empty.")
                }

                if (fileType.directoryContent == DirectoryContent.NotEmpty && !hasContent) {
                    throw ParsingException("Directory specified by $name is empty.")
                }
            }
        }

        return file
    }

    companion object : ExistingFileArgType()
}
