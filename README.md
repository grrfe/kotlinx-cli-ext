# kotlinx-cli-ext

Adds a File input parameter type, as well as "deferred commands", which can be executed at any point by the developer,
after the arguments have been
parsed
